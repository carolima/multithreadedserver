/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multithreadedserver;

import java.io.* ;
import java.net.* ;
import java.util.* ;

/**
 * Redes de Computadores
 * @author Carolina Lima
 */
public class MultithreadedServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        int port = 6789;
        
        ServerSocket welcomeSocket = new ServerSocket(port);
        
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            
            // Construct an object to process the HTTP request message.
            HttpRequest request = new HttpRequest(connectionSocket);

            // Create a new thread to process the request.
            Thread thread = new Thread(request);

            // Start the thread.
            thread.start();
            
        }
    }
    
}
